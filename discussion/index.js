const http = require('http');
const port = 4000;

// POST PUT AND DELETE methods/operation would not work in thye browser unlike the GET method, with Postman, it solves this problem by simulating a frontend for the developers to test their codes.
const server = http.createServer((req, res)=>{
	// http method of the incoming requests can be access via req.method
		// GET method - retrieving/reading information
	if(req.url === "/item" && req.method === "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data retreived from the database.");
	}
	if(req.url === "/items" && req.method === "PUT"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Update resources.");
	}
	if(req.url === "/items" && req.method === "POST"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data to be sent to the database.");
	}
	if(req.url === "/items" && req.method === "DELETE"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Delete resources.");
	}
})

server.listen(port);

console.log(`Server is running at local: ${port}`);