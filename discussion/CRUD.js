const http = require('http');

let database = [
{
	"name": "Brandon",
	"email": "brandon@mail.com"
},
{
	"name": "Jobert",
	"email": "jobert@mail.com"
}]

http.createServer((req, res)=>{
	//route for returning all items upon receiving a get req.
	if(req.url === "/users" && req.method === "GET"){
		res.writeHead(200, {"Content-Type": "_application/json"});
		// res.write() is used to print what is inside the parameters as a response.
		// Input has to be in a form a string that is why JSON.stringify is used.
		// data that will be received by the users/client from the server will be in a form of a stringified JSON.
		res.write(JSON.stringify(database));
		res.end();
	}
	if(req.url === "/users" && req.method === "POST"){
		let requestBody = "";
		/*
			data stream - sequence/flow of data
			Two Steps:
				data step - the data is received from the client and then is processed in the stream called "data".
					SYNTAX:
						req.on("data", function(data){
							requestBody += data;
						});
				end step - only runs after the request has completely been sent once the data has already been processed.

		*/
		req.on("data", function(data){
			requestBody += data;
		});
		req.on("end", function(){
			console.log(typeof requestBody)
			requestBody = JSON.parse(requestBody)
			console.log(typeof requestBody)
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}
			database.push(newUser);
			console.log(database)

			res.writeHead(200, {"Content-Type": "_application/json"});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}

}).listen(4000);

console.log("Server is running at localhost: 4000");